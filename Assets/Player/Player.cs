﻿using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.CrossPlatformInput;

namespace Player
{
	public class Player : NetworkBehaviour
	{
		private Vector3 _inputValue;

		// Update is called once per frame
		private void Update ()
		{
			if (!isLocalPlayer)
			{
				return;
			}
		
			_inputValue.x = CrossPlatformInputManager.GetAxis("Horizontal");
			_inputValue.y = 0f;
			_inputValue.z = CrossPlatformInputManager.GetAxis("Vertical");
		
			transform.Translate(_inputValue);
		}

		public override void OnStartLocalPlayer()
		{
			GetComponentInChildren<Camera>().enabled = true;
			GetComponentInChildren<AudioListener>().enabled = true;
		}
	}
}
