﻿using System;
using UnityEngine;
using UnityEngine.Networking;

namespace Game
{
	public class GameNetworkManager : NetworkManager
	{
	
		public void HostUp()
		{
			Debug.Log(Time.timeSinceLevelLoad + " (" + DateTime.Now + "): Starting network host");
			StartHost();
		}

		public override void OnStartHost()
		{
			Debug.Log(Time.timeSinceLevelLoad + " (" + DateTime.Now + "): Host started");
		}

		public override void OnStartClient(NetworkClient myClient)
		{
			Debug.Log(Time.timeSinceLevelLoad + " (" + DateTime.Now + "): Client Start Requested");
		}

		public override void OnClientConnect(NetworkConnection conn)
		{
			Debug.Log(Time.timeSinceLevelLoad + " (" + DateTime.Now + "): Client Conncected to " + conn.address);
		}
	}
}
